import winston from 'winston'
import DailyRotateFile from 'winston-daily-rotate-file'
import * as config from './config'
import * as io from './io'

let logDir = (config as any).logDir
if (!logDir) {
  logDir = io.getAppDir() + '/log'
}
io.ensureDir(logDir)

let logger = winston.createLogger({
  format: winston.format.combine(winston.format.splat(), winston.format.simple()),
  transports: [
    new winston.transports.Console({ level: 'debug', handleExceptions: true, debugStdout: true }),
    new DailyRotateFile({
      level: 'debug',
      dirname: logDir,
      filename: 'all_%DATE%.log',
      datePattern: 'YYYYMMDD',
      //datePattern: 'YYYY-MM-DD_HH_mm',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '28d'
    }),
    new DailyRotateFile({
      level: 'error',
      dirname: logDir,
      filename: 'error_%DATE%.log',
      datePattern: 'YYYYMMDD',
      //datePattern: 'YYYY-MM-DD_HH_mm',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '28d'
    })
  ]
})

export function log(message: string, ...meta: any[]) {
  logger.info(message, ...meta)
}

export function info(message: string, ...meta: any[]) {
  logger.info(message, ...meta)
}

export function debug(message: string, ...meta: any[]) {
  logger.debug(message, ...meta)
}

export function warn(message: string, ...meta: any[]) {
  logger.warn(message, ...meta)
}

export function error(message: string, ...meta: any[]) {
  logger.error(message, ...meta)
}
