"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const config = __importStar(require("./config"));
function fileExists(filePath) {
    try {
        fs_1.default.statSync(filePath);
        return true;
    }
    catch (e) {
        return false;
    }
}
exports.fileExists = fileExists;
function deleteFile(filePath) {
    try {
        fs_1.default.unlinkSync(filePath);
        return true;
    }
    catch (e) {
        return false;
    }
}
exports.deleteFile = deleteFile;
function getAppDir() {
    return config.$dir;
}
exports.getAppDir = getAppDir;
function ensureDir(dir) {
    if (!fs_1.default.existsSync(dir)) {
        let parentDir = path_1.default.dirname(path_1.default.resolve(dir));
        exports.ensureDir(parentDir);
        fs_1.default.mkdirSync(dir);
    }
}
exports.ensureDir = ensureDir;
function clearDir(dir) {
    for (let file of fs_1.default.readdirSync(dir)) {
        let filePath = path_1.default.join(dir, file);
        if (fs_1.default.statSync(filePath).isFile()) {
            fs_1.default.unlinkSync(filePath);
        }
    }
}
exports.clearDir = clearDir;
let tempUploadDir = '';
function getTempUploadDir() {
    if (!tempUploadDir) {
        tempUploadDir = path_1.default.join(exports.getAppDir(), 'upload');
        exports.ensureDir(tempUploadDir);
    }
    return tempUploadDir;
}
exports.getTempUploadDir = getTempUploadDir;
function readJSONFile(filename) {
    return new Promise((resolve, reject) => {
        fs_1.default.readFile(filename, {
            encoding: 'utf8'
        }, (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                try {
                    resolve(JSON.parse(data));
                }
                catch (e) {
                    reject(e);
                }
            }
        });
    });
}
exports.readJSONFile = readJSONFile;
function writeJSONFile(fileName, data) {
    return new Promise((resolve, reject) => {
        let text = JSON.stringify(data, null, 2);
        fs_1.default.writeFile(fileName, text, err => {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
}
exports.writeJSONFile = writeJSONFile;
function readJSONStream(stream) {
    return new Promise((resolve, reject) => {
        let content = '';
        stream.on('data', chunk => {
            content += chunk;
        });
        stream.on('end', () => {
            resolve(JSON.parse(content));
        });
        stream.on('error', error => {
            reject(error);
        });
        stream.setEncoding('utf-8');
        stream.resume();
    });
}
exports.readJSONStream = readJSONStream;
function createStreamPromise(stream) {
    let completed = false;
    return new Promise((resolve, reject) => {
        stream.on('error', error => {
            reject(error);
        });
        stream.on('end', () => {
            if (!completed) {
                resolve();
                completed = true;
            }
        });
        stream.on('finish', () => {
            if (!completed) {
                resolve();
                completed = true;
            }
        });
        stream.on('close', () => {
            if (!completed) {
                resolve();
                completed = true;
            }
        });
    });
}
exports.createStreamPromise = createStreamPromise;
//# sourceMappingURL=io.js.map