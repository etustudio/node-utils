"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const minimist_1 = __importDefault(require("minimist"));
const argv = minimist_1.default(process.argv.slice(2));
let configName = 'config.js';
let configDir = path_1.default.normalize(path_1.default.join(__dirname, '..'));
let configPath, filePath;
if (argv.c && typeof argv.c === 'string') {
    filePath = argv.c;
    filePath = path_1.default.resolve(process.cwd(), filePath);
    if (fs_1.default.existsSync(filePath)) {
        configPath = filePath;
    }
}
else {
    while (configDir) {
        filePath = path_1.default.join(configDir, configName);
        if (fs_1.default.existsSync(filePath)) {
            configPath = filePath;
            break;
        }
        if (configDir === '/') {
            configDir = '';
        }
        else {
            configDir = path_1.default.dirname(configDir);
        }
    }
}
let config = {};
if (configPath) {
    config = require(configPath);
    config.$name = path_1.default.basename(configPath);
    config.$dir = path_1.default.dirname(configPath);
    config.$path = configPath;
}
let typedConfig = config;
module.exports = typedConfig;
//# sourceMappingURL=config.js.map