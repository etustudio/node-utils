"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const config = __importStar(require("./config"));
exports.config = config;
__export(require("./io"));
__export(require("./logger"));
__export(require("./algorithm"));
__export(require("./web"));
const io = __importStar(require("./io"));
const logger = __importStar(require("./logger"));
const algorithm = __importStar(require("./algorithm"));
const web = __importStar(require("./web"));
exports.default = Object.assign(Object.assign(Object.assign(Object.assign({ config }, io), logger), algorithm), web);
// Allow error to be serialized.
Object.defineProperty(Error.prototype, 'toJSON', {
    value: function () {
        let alt = {};
        for (let key of Object.getOwnPropertyNames(this)) {
            alt[key] = this[key];
        }
        return alt;
    },
    configurable: true,
    enumerable: false,
    writable: true
});
//# sourceMappingURL=index.js.map