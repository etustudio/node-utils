"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const uuid_1 = __importDefault(require("uuid"));
const busboy_1 = __importDefault(require("busboy"));
const config = __importStar(require("./config"));
const logger = __importStar(require("./logger"));
const io = __importStar(require("./io"));
function jsonError(res, e) {
    let message = e.message || e;
    logger.error(`Error handling JSON request: %s`, message);
    if (e.stack) {
        logger.error(e.stack);
    }
    if (e.errors) {
        if (e.errors.length) {
            for (let error of e.errors) {
                logger.error('Error handling JSON request: %s', error.message);
            }
        }
        else {
            logger.error('Error handling JSON request: %s', e.errors);
        }
    }
    if (lodash_1.default.isString(e)) {
        // if error is simply a string, treat it as invalid argument error which would be displayed to client directly
        res.status(400).json({ message });
    }
    else {
        res.status(e.status || 500).json({
            message,
            error: process.env.PRODUCTION || config.production ? null : e.stack,
            key: e.key,
            data: e.data
        });
    }
}
exports.jsonError = jsonError;
class HttpError extends Error {
    constructor(status, message = '') {
        super(message);
        this.status = status;
    }
}
exports.HttpError = HttpError;
function isHttpError(err) {
    return lodash_1.default.isNumber(err.status);
}
exports.isHttpError = isHttpError;
class InvalidArgumentError extends HttpError {
    constructor(message = 'Invalid argument.', key = '') {
        super(400, message);
        this.key = key;
    }
}
exports.InvalidArgumentError = InvalidArgumentError;
class AuthorizationError extends HttpError {
    constructor(message = 'Not authorized.') {
        super(401, message);
    }
}
exports.AuthorizationError = AuthorizationError;
class PermissionError extends HttpError {
    constructor(message = 'Permission error.') {
        super(403, message);
    }
}
exports.PermissionError = PermissionError;
class ResourceNotFoundError extends HttpError {
    constructor(message = 'Resource not found.') {
        super(404, message);
    }
}
exports.ResourceNotFoundError = ResourceNotFoundError;
function logExpressError(e, req, res, next) {
    logger.error(`Error handling request at ${req.path}: ${e.message ? e.message : e}`);
    if (e.stack) {
        logger.error(e.stack);
    }
    if (e.errors) {
        for (let error of e.errors) {
            logger.error(`Error handling request: ${error.message}`);
        }
    }
    next(e);
}
exports.logExpressError = logExpressError;
function handleMultiPartForm(req, filesLimit = 1) {
    let result = {
        files: {},
        fields: {},
        filesLimitExceeded: false
    };
    let busboy = new busboy_1.default({
        headers: req.headers,
        limits: {
            files: filesLimit
        }
    });
    busboy.on('file', (fieldName, file, fileName, encoding, mimeType) => {
        logger.debug('File [%s] of field [%s], encoding [%s], mime type [%s]', fileName, fieldName, encoding, mimeType);
        fileName = decodeURIComponent(fileName);
        file.on('data', data => {
            logger.debug('File [%s] got %d bytes', fileName, data.length);
        });
        io.createStreamPromise(file).then(() => {
            logger.debug('File [%s] uploaded', fileName);
        });
        let resultFile = {};
        result.files[fieldName] = resultFile;
        resultFile.name = fileName;
        let generatedFileName = uuid_1.default.v4() + path_1.default.extname(fileName);
        let fullPath = path_1.default.join(io.getTempUploadDir(), generatedFileName);
        resultFile.path = fullPath;
        let stream = fs_1.default.createWriteStream(fullPath);
        resultFile.upload = io.createStreamPromise(file.pipe(stream)).then(() => {
            logger.debug('File [%s] saved', fileName);
        });
    });
    busboy.on('field', (fieldName, val, fieldNameTruncated, valTruncated) => {
        logger.debug('Field [%s], value: %s, field truncated: %s, value truncated: %s', fieldName, JSON.stringify(val), fieldNameTruncated, valTruncated);
        if (result.fields[fieldName] === undefined) {
            result.fields[fieldName] = val;
        }
        else if (!isArrayLike(result.fields[fieldName])) {
            result.fields[fieldName] = [result.fields[fieldName]];
            result.fields[fieldName].push(val);
        }
        else {
            result.fields[fieldName].push(val);
        }
    });
    busboy.on('filesLimit', () => {
        result.filesLimitExceeded = true;
    });
    return io
        .createStreamPromise(req.pipe(busboy))
        .then(() => {
        logger.debug('Done parsing form!');
        if (!Object.keys(result.files).length) {
            throw new InvalidArgumentError('Should upload a file.');
        }
        if (result.filesLimitExceeded) {
            for (let key of Object.keys(result.files)) {
                io.deleteFile(result.files[key].path);
            }
            throw new InvalidArgumentError('Should not upload more than ' + filesLimit + ' file.');
        }
        let promises = [];
        for (let key of Object.keys(result.files)) {
            promises.push(result.files[key].upload);
        }
        return Promise.all(promises);
    })
        .then(() => {
        return result;
    });
}
exports.handleMultiPartForm = handleMultiPartForm;
function isArrayLike(subject) {
    return subject && !!subject.length;
}
//# sourceMappingURL=web.js.map