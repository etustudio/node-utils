"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = __importDefault(require("crypto"));
function hash(content, algorithm) {
    let c = crypto_1.default.createHash(algorithm || 'sha256');
    c.update(content, 'utf8');
    return c.digest('hex');
}
exports.hash = hash;
function hashWithSalt(content, salt, algorithm) {
    let c = crypto_1.default.createHmac(algorithm || 'sha256', salt);
    c.update(content, 'utf8');
    return c.digest('hex');
}
exports.hashWithSalt = hashWithSalt;
function generateSalt(length) {
    return crypto_1.default.randomBytes(length || 128).toString('base64');
}
exports.generateSalt = generateSalt;
function encrypt(key, text) {
    let cipher = crypto_1.default.createCipher('aes-256-cbc', key);
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}
exports.encrypt = encrypt;
function decrypt(key, text) {
    let decipher = crypto_1.default.createDecipher('aes-256-cbc', key);
    let decrypted = decipher.update(text, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}
exports.decrypt = decrypt;
//# sourceMappingURL=algorithm.js.map