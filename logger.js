"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = __importDefault(require("winston"));
const winston_daily_rotate_file_1 = __importDefault(require("winston-daily-rotate-file"));
const config = __importStar(require("./config"));
const io = __importStar(require("./io"));
let logDir = config.logDir;
if (!logDir) {
    logDir = io.getAppDir() + '/log';
}
io.ensureDir(logDir);
let logger = winston_1.default.createLogger({
    format: winston_1.default.format.combine(winston_1.default.format.splat(), winston_1.default.format.simple()),
    transports: [
        new winston_1.default.transports.Console({ level: 'debug', handleExceptions: true, debugStdout: true }),
        new winston_daily_rotate_file_1.default({
            level: 'debug',
            dirname: logDir,
            filename: 'all_%DATE%.log',
            datePattern: 'YYYYMMDD',
            //datePattern: 'YYYY-MM-DD_HH_mm',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '28d'
        }),
        new winston_daily_rotate_file_1.default({
            level: 'error',
            dirname: logDir,
            filename: 'error_%DATE%.log',
            datePattern: 'YYYYMMDD',
            //datePattern: 'YYYY-MM-DD_HH_mm',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '28d'
        })
    ]
});
function log(message, ...meta) {
    logger.info(message, ...meta);
}
exports.log = log;
function info(message, ...meta) {
    logger.info(message, ...meta);
}
exports.info = info;
function debug(message, ...meta) {
    logger.debug(message, ...meta);
}
exports.debug = debug;
function warn(message, ...meta) {
    logger.warn(message, ...meta);
}
exports.warn = warn;
function error(message, ...meta) {
    logger.error(message, ...meta);
}
exports.error = error;
//# sourceMappingURL=logger.js.map