import _ from 'lodash'
import fs from 'fs'
import path from 'path'
import uuid from 'uuid'
import Busboy from 'busboy'
import { NextFunction, Request, Response } from 'express-serve-static-core'
import * as config from './config'
import * as logger from './logger'
import * as io from './io'
import { Readable } from 'stream'

export function jsonError(res: Response, e: any) {
  let message = e.message || e
  logger.error(`Error handling JSON request: %s`, message)
  if (e.stack) {
    logger.error(e.stack)
  }
  if (e.errors) {
    if (e.errors.length) {
      for (let error of e.errors) {
        logger.error('Error handling JSON request: %s', error.message)
      }
    } else {
      logger.error('Error handling JSON request: %s', e.errors)
    }
  }
  if (_.isString(e)) {
    // if error is simply a string, treat it as invalid argument error which would be displayed to client directly
    res.status(400).json({ message })
  } else {
    res.status(e.status || 500).json({
      message,
      error: process.env.PRODUCTION || (config as any).production ? null : e.stack,
      key: e.key,
      data: e.data
    })
  }
}

export class HttpError extends Error {
  public data?: any
  constructor(public status: number, message: string = '') {
    super(message)
  }
}

export function isHttpError(err: any): err is HttpError {
  return _.isNumber(err.status)
}

export class InvalidArgumentError extends HttpError {
  constructor(message: string = 'Invalid argument.', public key: string = '') {
    super(400, message)
  }
}

export class AuthorizationError extends HttpError {
  constructor(message: string = 'Not authorized.') {
    super(401, message)
  }
}

export class PermissionError extends HttpError {
  constructor(message: string = 'Permission error.') {
    super(403, message)
  }
}

export class ResourceNotFoundError extends HttpError {
  constructor(message: string = 'Resource not found.') {
    super(404, message)
  }
}

export function logExpressError(e: any, req: Request, res: Response, next: NextFunction) {
  logger.error(`Error handling request at ${req.path}: ${e.message ? e.message : e}`)
  if (e.stack) {
    logger.error(e.stack)
  }
  if (e.errors) {
    for (let error of e.errors) {
      logger.error(`Error handling request: ${error.message}`)
    }
  }
  next(e)
}

export function handleMultiPartForm(req: Request, filesLimit: number = 1) {
  let result = {
    files: {} as any,
    fields: {} as any,
    filesLimitExceeded: false
  }

  let busboy = new Busboy({
    headers: req.headers,
    limits: {
      files: filesLimit
    }
  })
  busboy.on('file', (fieldName: string, file: Readable, fileName: string, encoding: string, mimeType: string) => {
    logger.debug('File [%s] of field [%s], encoding [%s], mime type [%s]', fileName, fieldName, encoding, mimeType)
    fileName = decodeURIComponent(fileName)
    file.on('data', data => {
      logger.debug('File [%s] got %d bytes', fileName, data.length)
    })
    io.createStreamPromise(file).then(() => {
      logger.debug('File [%s] uploaded', fileName)
    })
    let resultFile: any = {}
    result.files[fieldName] = resultFile
    resultFile.name = fileName
    let generatedFileName = uuid.v4() + path.extname(fileName)
    let fullPath = path.join(io.getTempUploadDir(), generatedFileName)
    resultFile.path = fullPath
    let stream = fs.createWriteStream(fullPath)
    resultFile.upload = io.createStreamPromise(file.pipe(stream)).then(() => {
      logger.debug('File [%s] saved', fileName)
    })
  })
  busboy.on('field', (fieldName, val, fieldNameTruncated, valTruncated) => {
    logger.debug(
      'Field [%s], value: %s, field truncated: %s, value truncated: %s',
      fieldName,
      JSON.stringify(val),
      fieldNameTruncated,
      valTruncated
    )
    if (result.fields[fieldName] === undefined) {
      result.fields[fieldName] = val
    } else if (!isArrayLike(result.fields[fieldName])) {
      result.fields[fieldName] = [result.fields[fieldName]]
      result.fields[fieldName].push(val)
    } else {
      result.fields[fieldName].push(val)
    }
  })
  busboy.on('filesLimit', () => {
    result.filesLimitExceeded = true
  })

  return io
    .createStreamPromise(req.pipe(busboy) as any)
    .then(() => {
      logger.debug('Done parsing form!')
      if (!Object.keys(result.files).length) {
        throw new InvalidArgumentError('Should upload a file.')
      }
      if (result.filesLimitExceeded) {
        for (let key of Object.keys(result.files)) {
          io.deleteFile(result.files[key].path)
        }
        throw new InvalidArgumentError('Should not upload more than ' + filesLimit + ' file.')
      }
      let promises = []
      for (let key of Object.keys(result.files)) {
        promises.push(result.files[key].upload)
      }
      return Promise.all(promises)
    })
    .then(() => {
      return result
    })
}

function isArrayLike(subject: any) {
  return subject && !!subject.length
}
