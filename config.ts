import fs from 'fs'
import path from 'path'
import parseArgs from 'minimist'
import { Config } from '../types'
import { ConfigMeta } from './config-meta'

const argv = parseArgs(process.argv.slice(2))

let configName = 'config.js'
let configDir = path.normalize(path.join(__dirname, '..'))

let configPath, filePath
if (argv.c && typeof argv.c === 'string') {
  filePath = argv.c
  filePath = path.resolve(process.cwd(), filePath)
  if (fs.existsSync(filePath)) {
    configPath = filePath
  }
} else {
  while (configDir) {
    filePath = path.join(configDir, configName)
    if (fs.existsSync(filePath)) {
      configPath = filePath
      break
    }
    if (configDir === '/') {
      configDir = ''
    } else {
      configDir = path.dirname(configDir)
    }
  }
}

let config: any = {}

if (configPath) {
  config = require(configPath)
  config.$name = path.basename(configPath)
  config.$dir = path.dirname(configPath)
  config.$path = configPath
}

let typedConfig: Config & ConfigMeta = config

export = typedConfig
