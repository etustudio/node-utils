export interface ConfigMeta {
  $name: string
  $dir: string
  $path: string
}
