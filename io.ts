import path from 'path'
import fs from 'fs'

import * as config from './config'
import { Readable, Stream } from 'stream'

export function fileExists(filePath: string) {
  try {
    fs.statSync(filePath)
    return true
  } catch (e) {
    return false
  }
}

export function deleteFile(filePath: string) {
  try {
    fs.unlinkSync(filePath)
    return true
  } catch (e) {
    return false
  }
}

export function getAppDir() {
  return config.$dir
}

export function ensureDir(dir: string) {
  if (!fs.existsSync(dir)) {
    let parentDir = path.dirname(path.resolve(dir))
    exports.ensureDir(parentDir)
    fs.mkdirSync(dir)
  }
}

export function clearDir(dir: string) {
  for (let file of fs.readdirSync(dir)) {
    let filePath = path.join(dir, file)
    if (fs.statSync(filePath).isFile()) {
      fs.unlinkSync(filePath)
    }
  }
}

let tempUploadDir: string = ''

export function getTempUploadDir() {
  if (!tempUploadDir) {
    tempUploadDir = path.join(exports.getAppDir(), 'upload')
    exports.ensureDir(tempUploadDir)
  }
  return tempUploadDir
}

export function readJSONFile(filename: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.readFile(
      filename,
      {
        encoding: 'utf8'
      },
      (err, data) => {
        if (err) {
          reject(err)
        } else {
          try {
            resolve(JSON.parse(data))
          } catch (e) {
            reject(e)
          }
        }
      }
    )
  })
}

export function writeJSONFile(fileName: string, data: any): Promise<void> {
  return new Promise((resolve, reject) => {
    let text = JSON.stringify(data, null, 2)
    fs.writeFile(fileName, text, err => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

export function readJSONStream(stream: Readable) {
  return new Promise((resolve, reject) => {
    let content = ''
    stream.on('data', chunk => {
      content += chunk
    })
    stream.on('end', () => {
      resolve(JSON.parse(content))
    })
    stream.on('error', error => {
      reject(error)
    })
    stream.setEncoding('utf-8')
    stream.resume()
  })
}

export function createStreamPromise(stream: Stream) {
  let completed = false
  return new Promise<void>((resolve, reject) => {
    stream.on('error', error => {
      reject(error)
    })
    stream.on('end', () => {
      if (!completed) {
        resolve()
        completed = true
      }
    })
    stream.on('finish', () => {
      if (!completed) {
        resolve()
        completed = true
      }
    })
    stream.on('close', () => {
      if (!completed) {
        resolve()
        completed = true
      }
    })
  })
}
