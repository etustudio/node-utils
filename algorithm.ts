import crypto from 'crypto'

export function hash(content: string, algorithm?: string) {
  let c = crypto.createHash(algorithm || 'sha256')
  c.update(content, 'utf8')
  return c.digest('hex')
}

export function hashWithSalt(content: string, salt: string, algorithm?: string) {
  let c = crypto.createHmac(algorithm || 'sha256', salt)
  c.update(content, 'utf8')
  return c.digest('hex')
}

export function generateSalt(length?: number) {
  return crypto.randomBytes(length || 128).toString('base64')
}

export function encrypt(key: string, text: string) {
  let cipher = crypto.createCipher('aes-256-cbc', key)
  let crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex')
  return crypted
}

export function decrypt(key: string, text: string) {
  let decipher = crypto.createDecipher('aes-256-cbc', key)
  let decrypted = decipher.update(text, 'hex', 'utf8')
  decrypted += decipher.final('utf8')

  return decrypted
}
