import * as config from './config'
export { config }

export * from './io'
export * from './logger'
export * from './algorithm'
export * from './web'

import * as io from './io'
import * as logger from './logger'
import * as algorithm from './algorithm'
import * as web from './web'

export default { config, ...io, ...logger, ...algorithm, ...web }

// Allow error to be serialized.
Object.defineProperty(Error.prototype, 'toJSON', {
  value: function() {
    let alt: any = {}

    for (let key of Object.getOwnPropertyNames(this)) {
      alt[key] = this[key]
    }

    return alt
  },
  configurable: true,
  enumerable: false,
  writable: true
})
